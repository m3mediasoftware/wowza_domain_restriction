package com.hms.module;

import com.wowza.wms.amf.AMFDataList;
import com.wowza.wms.application.IApplicationInstance;
import com.wowza.wms.application.WMSProperties;
import com.wowza.wms.client.IClient;
import com.wowza.wms.module.IModuleOnApp;
import com.wowza.wms.module.IModuleOnConnect;
import com.wowza.wms.module.IModuleOnStream;
import com.wowza.wms.module.ModuleBase;
import com.wowza.wms.request.RequestFunction;
import com.wowza.wms.server.IServer;
import com.wowza.wms.server.Server;
import com.wowza.wms.stream.IMediaStream;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by Hugo Mercado on 14/09/2016.
 */
public class DomainRestricionModule extends ModuleBase implements IModuleOnApp {

    private String[] allowedDomains;
    private final String ALLOWED_DOMAIN_PROP = "AllowedDomains";

    @Override
    public void onAppStart(IApplicationInstance iApplicationInstance) {
        final WMSProperties props = Server.getInstance().getProperties();
        final String allowedDomainProp = props.getPropertyStr(ALLOWED_DOMAIN_PROP, null);
        if(allowedDomainProp == null) {
            return;
        }
        this.allowedDomains = allowedDomainProp.trim().split("\\s*,\\s*");
    }

    @Override
    public void onAppStop(IApplicationInstance iApplicationInstance) {
    }

    public void onConnect(IClient client, RequestFunction requestFunction, AMFDataList amfDataList) {
        String pageUrl = client.getProperties().getPropertyStr("connectpageUrl");
        if(pageUrl == null) {
            // this guy could be a publisher
            return;
        }
        if(this.allowedDomains == null || this.allowedDomains.equals("")) {
            client.rejectConnection();
            return;
        }

        URI uri = null;
        try {
            uri = new URI(pageUrl);
        } catch (URISyntaxException e) {
            getLogger().error("Can not identify URL.", e);
            client.rejectConnection();
            return;
        }
        if(uri == null) {
            getLogger().error("Could not identify URL.");
            client.rejectConnection();
            return;
        }
        String domain = uri.getHost();
        domain = domain.startsWith("www.") ? domain.substring(4) : domain;

        for(String allowedDomain : this.allowedDomains) {
            if(domain.equals(allowedDomain)) {
                return;
            }
        }
        getLogger().info("User from domain: " + domain + " is not allowed to play.");
        client.rejectConnection();
    }

}
